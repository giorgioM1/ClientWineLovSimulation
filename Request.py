import json

import urllib3

http = urllib3.PoolManager()
server = 'http://localhost:8080/'


class wrappedRequest:
    def __init__(self):
        pass

    @staticmethod
    def post(data, call):
        encoded_data = json.dumps(data).encode('utf-8')
        r = http.request('POST', server + call,
                         body=encoded_data,
                         headers={'Content-Type': 'application/json'})
        x = json.loads(r.data.decode('utf-8'))
        return x

    @staticmethod
    def get(call, value):
        r = http.request('GET', server + call+'/'+str(value), headers={'Content-Type': 'application/json'})
        return json.loads(r.data.decode('utf-8'))
