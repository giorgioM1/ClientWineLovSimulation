import random

from Request import wrappedRequest

wr = wrappedRequest()


class createValues:
    def __init__(self):
        pass

    @staticmethod
    def createAnswer(u):
        data = {'user': u}

        for i in range(1, 11):
            question = "" + str(i) + ""
            r = random.random()
            if r < 0.51:
                ans = 'A'
            else:
                ans = 'B'
            data[question] = ans
        print data
        return data

    @staticmethod
    def createRating():
        r = random.random()
        rating = 0
        if r <= 0.6:
            nr = random.random()
            if nr <= 0.75:
                rating = random.randint(3, 5)
            else:
                rating = random.randint(1, 2)

        return rating

    # this function create a fake response of the questions and ask the wine
    def makeRequestTaste(self, u):
        data = self.createAnswer(u)
        return wr.post(data, 'getTastes')

    # create the rating for a wine and send the response
    def createDataNewRating(self, u, w):
        r = self.createRating()
        data = {'user_id': u, 'wine_id': w, 'rating': r}
        return data

    def rateWine(self, u, w):
        data = self.createDataNewRating(u, w)
        if data['rating'] is not 0:
            print w
            return wr.post(data, 'addRating')

    def addUser(self, nameUser):
        data = {'name': nameUser}
        return wr.post(data, 'addUser')

    def getSimilarityAndRating(self, idrandom):
        data = wr.get('getSimilarity', idrandom)
        print data
        for i in data[2]:
            self.rateWine(idrandom, i)

    def getSimilarTaste(self, idrandom):
        data = wr.get('getTasteTips', idrandom)
        print data
        for element in data:
            for idwine in element['id']:
                self.rateWine(idrandom, idwine)


if __name__ == '__main__':
    pass
